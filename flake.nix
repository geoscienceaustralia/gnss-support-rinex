{
  inputs = {
    gnss-common.url = "git+https://bitbucket.org/geoscienceaustralia/gnss-common";
    nixpkgs.follows = "gnss-common/nixpkgs";
  };

  outputs = {
    nixpkgs,
    gnss-common,
    ...
  }: let
    project = "gnss-support-rinex";
    forAllSystems = f:
      nixpkgs.lib.genAttrs
      gnss-common.lib.systems
      (
        system:
          f {
            pkgs = nixpkgs.legacyPackages.${system}.extend gnss-common.overlays.default;
            inherit system;
          }
      );
  in {
    formatter = forAllSystems ({system, ...}: gnss-common.formatter.${system});

    devShells = forAllSystems ({pkgs, ...}: rec {
      # Environment for custom pipeline `build-pipelines-docker-image`
      ciImage = pkgs.mkShellNoCC {
        name = "${project}-ci-image";

        packages = [
          # gnss-common overlay
          pkgs.docker-login
        ];

        shellHook = ''
          export PROJECT="${project}"
        '';
      };

      # Environment for all pipelines (except `build-pipelines-docker-image`)
      ci = pkgs.mkShellNoCC {
        name = "${project}-ci";

        inputsFrom = [ciImage];

        packages = [
          # nixpkgs
          pkgs.bash
          pkgs.awscli2
          pkgs.docker
          (pkgs.maven.override { jdk = pkgs.jdk21; })
          pkgs.jdk21
        ];
      };

      # Environment for developer workstations
      developer = pkgs.mkShellNoCC {
        name = project;

        inputsFrom = [ci];

        packages = [
        ];
      };

      default = developer;
    });
  };
  nixConfig.bash-prompt-prefix = "(nix-shell:$name) ";
}
