package au.gov.ga.gnss.support.rinex;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.base.Joiner;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.MutableTriple;
import org.testng.annotations.Test;

public class RinexFileHeaderTest {

    private InputStream resourceAsStream(String fileName) {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName);
    }

    private RinexFileHeader parseRinexDataHeader(String fileName) throws Exception {
        return new RinexFileHeader(this.resourceAsStream(fileName));
    }

    @Test
    public void extractRinex2DataHeaderObs() throws Exception {
        String rinexFileName = "bala3080.17o";
        RinexFileHeader rinexDataHeader = this.parseRinexDataHeader(rinexFileName);

        assertThat(rinexDataHeader.getAntennaMarkerArp()).isEqualTo(
            new MutableTriple<Double, Double, Double>(0.0, 1.0, 2.0));

        assertThat(rinexDataHeader.getAntennaSerialNumber()).isEqualTo("09310004");
        assertThat(rinexDataHeader.getAntennaType()).isEqualTo("LEIAR25.R3      NONE");

        assertThat(rinexDataHeader.getApproxPositionXyz()).isEqualTo(
            new ImmutableTriple<Double, Double, Double>(-3002033.78, 4472882.648, -3403723.89));

        assertThat(rinexDataHeader.getMarkerName()).isEqualTo("BALA");
        assertThat(rinexDataHeader.getMarkerNumber()).isEqualTo("59947M001");
        assertThat(rinexDataHeader.getReceiverSerialNumber()).isEqualTo("5306K50694");
        assertThat(rinexDataHeader.getReceiverType()).isEqualTo("TRIMBLE NETR9");
        assertThat(rinexDataHeader.getReceiverFirmwareVersion()).isEqualTo("5.15");
        assertThat(rinexDataHeader.getRinexVersion()).isEqualTo("2.11");
    }

    @Test
    public void extractRinex2DataHeaderNav() throws Exception {
        String rinexFileName = "bboo3080.17g.Z";
        RinexFileHeader rinexDataHeader = this.parseRinexDataHeader(rinexFileName);
        assertThat(rinexDataHeader.getRinexVersion()).isEqualTo("2.10");
    }

    @Test
    public void extractRinex3DataHeaderMet() throws Exception {
        String rinexFileName = "ANDA00AUS_R_20170250000_01D_30S_MM.rnx.gz";
        RinexFileHeader rinexDataHeader = this.parseRinexDataHeader(rinexFileName);
        assertThat(rinexDataHeader.getRinexVersion()).isEqualTo("3.02");
    }

    @Test
    public void validHeader() throws Exception {
        byte[] rinexFileContent = "     2.11           OBSERVATION DATA    G (GPS)             RINEX VERSION / TYPE\n".getBytes();
        new RinexFileHeader(new ByteArrayInputStream(rinexFileContent));
    }

    @Test
    public void invalidHeader() throws Exception {
        byte[] rinexFileContent = "     2.11           OBSERVATION DATA    G (GPS)             RINEX VERSION / TYP\n".getBytes();
        assertThat(catchThrowable(() -> new RinexFileHeader(new ByteArrayInputStream(rinexFileContent))))
            .isInstanceOf(BadRinexFileHeaderException.class)
            .hasMessage("Bad header, missing RINEX version");
    }

    @Test
    public void overwriteAllFields() throws Exception {
        RinexFileHeader header = new RinexFileHeader(new ByteArrayInputStream((
              "     2.12           OBSERVATION DATA    M (MIXED)           RINEX VERSION / TYPE\n"
            + "teqc  2016Nov7                          20171105 00:09:34UTCPGM / RUN BY / DATE\n"
            + "Solaris x86 5.10|AMD64|cc SC5.8 -xarch=amd64|=+|=+          COMMENT\n"
            + "   0.000      (antenna height)                              COMMENT\n"
            + " -32.46078401 (latitude)                                    COMMENT\n"
            + "+123.86804773 (longitude)                                   COMMENT\n"
            + "0130.871      (elevation)                                   COMMENT\n"
            + "BIT 2 OF LLI FLAGS DATA COLLECTED UNDER A/S CONDITION       COMMENT\n"
            + "59947M001 (COGO code)                                       COMMENT\n"
            + "BALA                                                        MARKER NAME\n"
            + "59947M001                                                   MARKER NUMBER\n"
            + "geodesy@ga.gov.au   GEOSCIENCE AUSTRALIA                    OBSERVER / AGENCY\n"
            // Receiver info is intentionally omitted, it will be added below
            // + "5306K50694          TRIMBLE NETR9       5.15                REC # / TYPE / VERS\n"
            + "antenna sn          LEIAR25.R3      NONE                    ANT # / TYPE\n"
            + " -3002033.7800  4472882.6480 -3403723.8900                  APPROX POSITION XYZ\n"
            + "        0.0000        1.0000        2.0000                  ANTENNA: DELTA H/E/N\n"
            + "     1     1                                                WAVELENGTH FACT L1/2\n"
            + "     7    L1    L2    C1    P1    P2    S1    S2            # / TYPES OF OBSERV\n"
            + "    30.0000                                                 INTERVAL\n"
            + "    18                                                      LEAP SECONDS\n"
            + "Forced Modulo Decimation to 30 seconds                      COMMENT\n"
            + " SNR is mapped to RINEX snr flag value [0-9]                COMMENT\n"
            + "  L1 & L2: min(max(int(snr_dBHz/6), 0), 9)                  COMMENT\n"
            + "  2017    11     4     0     0    0.0000000     GPS         TIME OF FIRST OBS\n"
            + "                                                            END OF HEADER\n")
            .getBytes()
        ));
        header.setRinexVersion("2.12");
        header.setMarkerName("ABCD");
        header.setMarkerNumber("marker number");
        header.getApproxPositionXyz().setLeft(1.0);
        header.getApproxPositionXyz().setMiddle(2.0);
        header.getApproxPositionXyz().setRight(3.0);
        header.setReceiverSerialNumber("receiver sn");
        header.setReceiverType("receiver type");
        header.setReceiverFirmwareVersion("firmware version");
        header.setAntennaSerialNumber("antenna sn");
        header.setAntennaType("antenna type");
        header.getAntennaMarkerArp().setLeft(2.0);
        header.getAntennaMarkerArp().setMiddle(3.0);
        header.getAntennaMarkerArp().setRight(4.0);

        String time = this.time();

        StringWriter output = new StringWriter();
        PrintWriter outputWriter = new PrintWriter(output);

        List<String> overwrittenFields = header.write(outputWriter, time);

        assertThat(overwrittenFields).isEqualTo(Arrays.asList(
            "MARKER NAME",
            "MARKER NUMBER",
            "ANT TYPE",
            "APPROX POSITION X",
            "APRROX POSITION Y",
            "APRROX POSITION Z",
            "ANTENNA DELTA H",
            "ANTENNA DELTA E",
            "ANTENNA DELTA H",
            "REC #",
            "REC TYPE",
            "REC VERS"
        ));

        StringReader input = new StringReader(output.toString());

        List<String> actualHeaderLines = new BufferedReader(input).lines().collect(Collectors.toList());

        List<String> expectedHeaderLines = Arrays.asList(
            "     2.12           OBSERVATION DATA    M (MIXED)           RINEX VERSION / TYPE",
            "GA: Modified by Geoscience Australia, "     + time +    "   COMMENT",
            "GA: Replaced MARKER NAME, BALA                              COMMENT",
            "GA: Replaced MARKER NUMBER, 59947M001                       COMMENT",
            "GA: Replaced ANT TYPE, LEIAR25.R3      NONE                 COMMENT",
            "GA: Replaced APPROX POSITION X, -3002033.78                 COMMENT",
            "GA: Replaced APRROX POSITION Y, 4472882.648                 COMMENT",
            "GA: Replaced APRROX POSITION Z, -3403723.89                 COMMENT",
            "GA: Replaced ANTENNA DELTA H, 0.0                           COMMENT",
            "GA: Replaced ANTENNA DELTA E, 1.0                           COMMENT",
            "GA: Replaced ANTENNA DELTA H, 2.0                           COMMENT",
            "GA: Added REC #, receiver sn                                COMMENT",
            "GA: Added REC TYPE, receiver type                           COMMENT",
            "GA: Added REC VERS, firmware version                        COMMENT",
            "receiver sn         receiver type       firmware version    REC # / TYPE / VERS",
            "teqc  2016Nov7                          20171105 00:09:34UTCPGM / RUN BY / DATE",
            "Solaris x86 5.10|AMD64|cc SC5.8 -xarch=amd64|=+|=+          COMMENT",
            "   0.000      (antenna height)                              COMMENT",
            " -32.46078401 (latitude)                                    COMMENT",
            "+123.86804773 (longitude)                                   COMMENT",
            "0130.871      (elevation)                                   COMMENT",
            "BIT 2 OF LLI FLAGS DATA COLLECTED UNDER A/S CONDITION       COMMENT",
            "59947M001 (COGO code)                                       COMMENT",
            "ABCD                                                        MARKER NAME",
            "marker number                                               MARKER NUMBER",
            "geodesy@ga.gov.au   GEOSCIENCE AUSTRALIA                    OBSERVER / AGENCY",
            "antenna sn          antenna type                            ANT # / TYPE",
            "        1.0000        2.0000        3.0000                  APPROX POSITION XYZ",
            "        2.0000        3.0000        4.0000                  ANTENNA: DELTA H/E/N",
            "     1     1                                                WAVELENGTH FACT L1/2",
            "     7    L1    L2    C1    P1    P2    S1    S2            # / TYPES OF OBSERV",
            "    30.0000                                                 INTERVAL",
            "    18                                                      LEAP SECONDS",
            "Forced Modulo Decimation to 30 seconds                      COMMENT",
            " SNR is mapped to RINEX snr flag value [0-9]                COMMENT",
            "  L1 & L2: min(max(int(snr_dBHz/6), 0), 9)                  COMMENT",
            "  2017    11     4     0     0    0.0000000     GPS         TIME OF FIRST OBS",
            "                                                            END OF HEADER"
        );
        assertThat(actualHeaderLines).isEqualTo(expectedHeaderLines);
    }

    @Test
    public void overwriteOneField() throws Exception {
        RinexFileHeader header = new RinexFileHeader(new BufferedReader(new StringReader(
              "     2.11           OBSERVATION DATA    M (MIXED)           RINEX VERSION / TYPE\n"
            + "                                                            MARKER NAME\n"
            + "        1.0000        2.0000        3.0000                  APPROX POSITION XYZ\n"
            + "                                                            END OF HEADER\n"
        )));
        header.setMarkerName("ABCD");

        String time = this.time();

        StringWriter outputWriter = new StringWriter();
        List<String> overwrittenFields = header.write(outputWriter, time);

        assertThat(overwrittenFields).isEqualTo(Arrays.asList(
            "MARKER NAME"
        ));

        assertThat(outputWriter.toString()).isEqualTo(Joiner.on('\n').join(
            "     2.11           OBSERVATION DATA    M (MIXED)           RINEX VERSION / TYPE",
            "GA: Modified by Geoscience Australia, "     + time +    "   COMMENT",
            "GA: Replaced MARKER NAME, null                              COMMENT",
            "ABCD                                                        MARKER NAME",
            "        1.0000        2.0000        3.0000                  APPROX POSITION XYZ",
            "                                                            END OF HEADER\n"
        ));
    }

    /**
     * Deleting fields is intentially not supported.
     */
    @Test
    public void deleteOneField() throws Exception {
        String headerText = Joiner.on('\n').join(
            "     2.11           OBSERVATION DATA    M (MIXED)           RINEX VERSION / TYPE",
            "        1.0000        2.0000        3.0000                  APPROX POSITION XYZ",
            "                                                            END OF HEADER\n"
        );
        RinexFileHeader header = new RinexFileHeader(new BufferedReader(new StringReader(
            headerText
        )));
        header.getApproxPositionXyz().setLeft(null);

        StringWriter outputWriter = new StringWriter();
        List<String> overwrittenFields = header.write(outputWriter, this.time());

        assertThat(overwrittenFields).isEmpty();
        assertThat(outputWriter.toString()).isEqualTo(headerText);
    }

    @Test
    public void addOneLine() throws Exception {
        RinexFileHeader header = new RinexFileHeader(new BufferedReader(new StringReader(
              "     2.11           OBSERVATION DATA    M (MIXED)           RINEX VERSION / TYPE\n"
            + "        1.0000        2.0000        3.0000                  APPROX POSITION XYZ\n"
            + "                                                            END OF HEADER\n"
        )));
        header.setMarkerName("ABCD");

        String time = this.time();
        StringWriter outputWriter = new StringWriter();
        List<String> overwrittenFields = header.write(outputWriter, time);

        assertThat(overwrittenFields).isEqualTo(Arrays.asList(
            "MARKER NAME"
        ));

        assertThat(outputWriter.toString()).isEqualTo(Joiner.on('\n').join(
            "     2.11           OBSERVATION DATA    M (MIXED)           RINEX VERSION / TYPE",
            "GA: Modified by Geoscience Australia, "     + time +    "   COMMENT",
            "GA: Added MARKER NAME, ABCD                                 COMMENT",
            "ABCD                                                        MARKER NAME",
            "        1.0000        2.0000        3.0000                  APPROX POSITION XYZ",
            "                                                            END OF HEADER\n"
        ));
    }

    @Test
    public void generateHeader() throws Exception {
        RinexFileHeader header = new RinexFileHeader();

        header.setMarkerName("CUT0");
        header.setMarkerNumber("59945M001");
        header.setAgency("Curtin University");

        header.setReceiverSerialNumber("5023K67889");
        header.setReceiverType("TRIMBLE NETR9");
        header.setReceiverFirmwareVersion("5.22");

        header.setAntennaSerialNumber("4928353386");
        header.setAntennaType("TRM59800.00     SCIS");

        header.getApproxPositionXyz().setLeft(-2364337.2699);
        header.getApproxPositionXyz().setMiddle(4870285.562401234);
        header.getApproxPositionXyz().setRight(-3360809.8398000001234);

        header.getAntennaMarkerArp().setLeft(0.0);
        header.getAntennaMarkerArp().setMiddle(0.0);
        header.getAntennaMarkerArp().setRight(0.000001234);

        StringWriter outputWriter = new StringWriter();
        header.write(outputWriter);

        assertThat(outputWriter.toString()).isEqualTo(Joiner.on('\n').join(
            "CUT0                                                        MARKER NAME",
            "59945M001                                                   MARKER NUMBER",
            "                    Curtin University                       OBSERVER / AGENCY",
            "5023K67889          TRIMBLE NETR9       5.22                REC # / TYPE / VERS",
            "4928353386          TRM59800.00     SCIS                    ANT # / TYPE",
            " -2364337.2699  4870285.5624 -3360809.8398                  APPROX POSITION XYZ",
            "        0.0000        0.0000        0.0000                  ANTENNA: DELTA H/E/N",
            "                                                            END OF HEADER\n"
        ));
    }

    @Test
    public void overwriteNone() throws Exception {
        String headerText = Joiner.on('\n').join(
            "     2.11           OBSERVATION DATA    M (MIXED)           RINEX VERSION / TYPE",
            "                                                            MARKER NAME",
            "        1.0000        2.0000        3.0000                  APPROX POSITION XYZ",
            "                                                            END OF HEADER\n"
        );
        RinexFileHeader header = new RinexFileHeader(new BufferedReader(new StringReader(
            headerText
        )));
        StringWriter outputWriter = new StringWriter();
        List<String> overwrittenFields = header.write(outputWriter, this.time());

        assertThat(overwrittenFields).isEmpty();
        assertThat(outputWriter.toString()).isEqualTo(headerText);
    }

    @Test
    public void overwriteWithInvalidLine() throws Exception {
        String headerText = Joiner.on('\n').join(
            "     2.11           OBSERVATION DATA    M (MIXED)           RINEX VERSION / TYPE",
            "                                                            MARKER NAME",
            "        1.0000        2.0000        3.0000                  APPROX POSITION XYZ",
            "                                                            END OF HEADER\n"
        );
        RinexFileHeader header = new RinexFileHeader(new BufferedReader(new StringReader(
            headerText
        )));
        // Firmware version must be 20 characters or less
        header.setReceiverFirmwareVersion("012345678901234567890");
        StringWriter outputWriter = new StringWriter();

        assertThat(catchThrowable(() -> header.write(outputWriter, this.time())))
            .isInstanceOf(BadRinexFileHeaderException.class)
            .hasMessageStartingWith("Overwritting would result in an invalid header line");
    }

    @Test
    public void overwriteFile() throws Exception {
        String fileContent = Joiner.on('\n').join(
            "     2.11           OBSERVATION DATA    G (GPS)             RINEX VERSION / TYPE",
            "BALA                                                        MARKER NAME",
            "                                                            END OF HEADER",
            "etc.\n"
        );

        Reader inputReader = new StringReader(fileContent);
        Writer outputWriter = new StringWriter();

        String time = this.time();

        List<String> overwrittenFields = RinexFileHeader.overwrite(
            inputReader,
            outputWriter,
            header -> {
                header.setMarkerNumber("marker number");
            },
            time
        );

        assertThat(overwrittenFields).isEqualTo(Arrays.asList(
            "MARKER NUMBER"
        ));

        assertThat(outputWriter.toString()).isEqualTo(Joiner.on('\n').join(
            "     2.11           OBSERVATION DATA    G (GPS)             RINEX VERSION / TYPE",
            "GA: Modified by Geoscience Australia, "     + time +    "   COMMENT",
            "GA: Added MARKER NUMBER, marker number                      COMMENT",
            "marker number                                               MARKER NUMBER",
            "BALA                                                        MARKER NAME",
            "                                                            END OF HEADER",
            "etc.\n"
        ));
    }

    private String time() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
            .withZone(ZoneId.of("UTC"))
            .format(Instant.now());
    }
}
