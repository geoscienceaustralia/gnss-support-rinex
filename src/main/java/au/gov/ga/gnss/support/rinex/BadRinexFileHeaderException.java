package au.gov.ga.gnss.support.rinex;

public class BadRinexFileHeaderException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public BadRinexFileHeaderException(String message) {
        super(message);
    }

    public BadRinexFileHeaderException(String message, Throwable x) {
        super(message, x);
    }
}

