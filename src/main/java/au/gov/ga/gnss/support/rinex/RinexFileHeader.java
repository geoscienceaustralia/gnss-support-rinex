package au.gov.ga.gnss.support.rinex;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import com.google.common.collect.Lists;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.tuple.MutableTriple;
import org.springframework.util.StringUtils;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
public class RinexFileHeader implements Serializable {

    /**
     * Header lines we would like to parse
     */
    private static HeaderLine[] candidateLines = {
        HeaderLine.rinexVersion,
        HeaderLine.markerName,
        HeaderLine.markerNumber,
        HeaderLine.observerAndAgency,
        HeaderLine.receiverId,
        HeaderLine.antennaId,
        HeaderLine.approxPositionXyz,
        HeaderLine.antennaMarkerArp,
        HeaderLine.endOfHeader
    };

    @Setter
    @Getter
    private String rinexVersion;

    @Setter
    @Getter
    private String markerName;

    @Setter
    @Getter
    private String markerNumber;

    @Setter
    @Getter
    private String observer;

    @Setter
    @Getter
    private String agency;

    @Setter
    @Getter
    private MutableTriple<Double, Double, Double> approxPositionXyz = new MutableTriple<>();

    @Setter
    @Getter
    private String receiverSerialNumber;

    @Setter
    @Getter
    private String receiverType;

    @Setter
    @Getter
    private String receiverFirmwareVersion;

    @Setter
    @Getter
    private String antennaSerialNumber;

    @Setter
    @Getter
    private String antennaType;

    @Setter
    @Getter
    private MutableTriple<Double, Double, Double> antennaMarkerArp = new MutableTriple<>();

    /**
     * We will save the state of the header immediately after parsing,
     * so that we can report on quote header fields values.
     */
    private transient RinexFileHeader old;

    /**
     * Complete header text
     */
    private transient List<String> headerText = new ArrayList<>();

    /**
     * We keep track of what candidate lines remain unmatched after parsing a header,
     * so that we know what new header lines need to be inserted when output a header.
     */
    private transient List<HeaderLine> unmatchedCandidateLines = Lists.newArrayList(RinexFileHeader.candidateLines);

    public RinexFileHeader() {
    }

    public RinexFileHeader(String rinexFileContent) {
        this(rinexFileContent.getBytes());
    }

    public RinexFileHeader(byte[] rinexFileContent) {
        this(new ByteArrayInputStream(rinexFileContent));
    }

    public RinexFileHeader(InputStream rinexFileContent) {
        this(new BufferedReader(new InputStreamReader(
            RinexCompression.decompress(rinexFileContent)
        )));
    }

    public RinexFileHeader(BufferedReader reader) {
        reader.lines()
            .map(String::stripTrailing)
            .peek(headerText::add)
            .peek(line ->
                Stream.of(candidateLines).forEach(candidate -> {
                    if (candidate.parseLine(this, line)) {
                        this.unmatchedCandidateLines.remove(candidate);
                    }
                })
            )
            .filter(line -> HeaderLine.endOfHeader.parseLine(line).matches())
            .findFirst();

        if (!StringUtils.hasLength(this.rinexVersion)) {
            throw new BadRinexFileHeaderException("Bad header, missing RINEX version");
        }

        this.old = SerializationUtils.clone(this);
    }

    private String commentLine(String commentText) {
        return HeaderLine.comment.generateLine(new RinexFileHeader() {
            @Getter
            String comment = commentText;
        });
    }

    public String toString() {
        try {
            StringWriter writer = new StringWriter();
            write(writer);
            return writer.toString();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public void write(Writer outputWriter) throws IOException {
        this.write(outputWriter, RinexFileHeader.time());
    }

    /**
     * Write out a complete RINEX header to the supplied writer.
     *
     * @return List of overwritten fields, e.g., ["MARKER NUMBER", "REC #"]
     */
    List<String> write(Writer outputWriter, String time) throws IOException {

        StringWriter buffer = new StringWriter();
        PrintWriter bufferWriter = new PrintWriter(buffer);
        PrintWriter outputPrinter = new PrintWriter(outputWriter);

        List<String> overwrittenFields = new ArrayList<>();

        List<HeaderLine> modified = new ArrayList<>();

        for (String inputLine : this.headerText) {
            String outputLine = null;
            for (HeaderLine candidate : RinexFileHeader.candidateLines) {
                outputLine = candidate.overwriteLine(this, inputLine);
                if (outputLine != null) {
                    if (!outputLine.equals(inputLine)) {
                        modified.add(candidate);
                        break;
                    } else if (outputLine != inputLine) {
                        break;
                    }
                }
            };
            bufferWriter.println(outputLine);
        }
        bufferWriter.flush();

        List<String> preamble = new ArrayList<>();

        // comment on updated fields
        for (HeaderLine modifiedLine : modified) {
            for (HeaderField<?> field : modifiedLine.getFields()) {
                Object oldValue = field.getValue(old);
                Object newValue = field.getValue(this);
                if (!newValue.equals(oldValue)) {
                    preamble.add(this.commentLine("GA: Replaced " + field.getDisplayName() + ", " + oldValue));
                    overwrittenFields.add(field.getDisplayName());
                }
            }
        }

        // comment on new fields
        if (this.old != null) {
            for (HeaderLine newLine : this.unmatchedCandidateLines) {
                for (HeaderField<?> field : newLine.getFields()) {
                    if (field.getValue(this) != null) {
                        preamble.add(this.commentLine("GA: Added " + field.getDisplayName() + ", " + field.getValue(this)));
                        overwrittenFields.add(field.getDisplayName());
                    }
                }
            }
        }
        // insert new lines
        for (HeaderLine newLine : this.unmatchedCandidateLines) {
            String outputLine = newLine.generateLine(this);
            if (outputLine != null) {
                preamble.add(outputLine);
            }
        }

        Stream<String> lines = new BufferedReader(new StringReader(buffer.toString())).lines();

        if (preamble.isEmpty()) {
            lines.forEach(outputPrinter::println);
        } else {
            if (this.old == null) {
                preamble.forEach(outputPrinter::println);
            } else {
                lines.forEach(line -> {
                    outputPrinter.println(line);

                    // insert preamble after RINEX version line
                    if (HeaderLine.rinexVersion.parseLine(line).matches()) {
                        outputPrinter.println(this.commentLine("GA: Modified by Geoscience Australia, " + time));
                        preamble.forEach(outputPrinter::println);
                    }
                });
            }
        }
        outputPrinter.flush();
        return overwrittenFields;
    }

    /**
     * Copy a RINEX file, overwritting its header.
     *
     * @param inputReader input RINEX file source
     * @param outputWriter output RINEX file destination
     * @param updateHeader callback to update the RINEX file header
     *
     * @return List of overwritten fields, e.g., ["MARKER NUMBER", "REC #"]
     */
    public static List<String> overwrite(
        Reader inputReader,
        Writer outputWriter,
        Consumer<RinexFileHeader> updateHeader
    ) throws IOException {

        return RinexFileHeader.overwrite(
            inputReader,
            outputWriter,
            updateHeader,
            RinexFileHeader.time()
        );
    }

    static List<String> overwrite(
        Reader inputReader,
        Writer outputWriter,
        Consumer<RinexFileHeader> updateHeader,
        String time
    ) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(inputReader);
        PrintWriter outputPrinter = new PrintWriter(outputWriter);

        RinexFileHeader header = new RinexFileHeader(bufferedReader);
        updateHeader.accept(header);

        List<String> overwrittenFields = header.write(outputWriter, time);
        bufferedReader.lines().forEach(outputPrinter::println);
        outputPrinter.flush();

        return overwrittenFields;
    }

    private static String time() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
            .withZone(ZoneId.of("UTC"))
            .format(Instant.now());
    }
}
