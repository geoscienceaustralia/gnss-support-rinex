package au.gov.ga.gnss.support.rinex;

import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.regex.Matcher;

import org.apache.commons.beanutils.ConstructorUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.util.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Description of a single header field
 */
@AllArgsConstructor
class HeaderField<T> {

    // TODO: try to eliminate groupNumber
    /**
     * Regex capture group offset with header line pattern
     */
    @Getter
    private int groupNumber;

    /**
     * Field name must match a nested property of RinexFileHeader class
     */
    @Getter
    private String name;

    /**
     * Field name as displayed in RINEX header
     */
    @Getter
    private String displayName;

    /**
     * Field value type
     */
    @Getter
    private Class<T> type;

    /**
     * Field value padding within the header line
     */
    @Getter
    private int padding;

    /**
     * Regex capture group names cannot contain dots.
     */
    // TODO: how can we avoid relying on special value 'XXXX'?
    public String getGroupName() {
        return this.getName().replaceAll("\\.", "XXXX");
    }

    @Override
    public String toString() {
        return this.name;
    }

    /**
     * Extract this field's value from supplied header.
     */
    @SuppressWarnings("unchecked")
    public T getValue(RinexFileHeader header) {
        return (T) PropertyAccessorFactory.forBeanPropertyAccess(header).getPropertyValue(this.getName());
    }

    /**
     * Extract and format this field's value from the supplied header.
     */
    public String getFormattedValue(RinexFileHeader header) {
        T value = this.getValue(header);
        return value == null ? "" : String.valueOf(value);
    }

    /**
     * Update the supplied header with the field value contained in the supplied matcher.
     */
    public void setValue(RinexFileHeader header, Matcher matcher) {
        String trimmed = matcher.group(this.getGroupName()).trim();
        if (!StringUtils.hasLength(trimmed)) {
            return;
        }
        try {
            this.setValue(
                header,
                (T) ConstructorUtils.invokeConstructor(
                    this.getType(),
                    new Object[] { trimmed },
                    new Class[] { String.class }
                )
            );
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Update the supplied header with the supplied field value.
     */
    public void setValue(RinexFileHeader header, T value) {
        try {
            PropertyUtils.setProperty(header, this.getName(), value);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Decimal field, formatted to four decimal places
     */
    public static class DecimalField extends HeaderField<Double> {
        public DecimalField(int group, String name, String displayName, int padding) {
            super(group, name, displayName, Double.class, padding);
        }

        @Override
        public String getFormattedValue(RinexFileHeader header) {
            Double value = super.getValue(header);
            return value == null ? "" : new DecimalFormat("0.0000").format(super.getValue(header));
        }
    }

    public static HeaderField<String> string(int group, String name, String displayName, int padding) {
        return new HeaderField<String>(group, name, displayName, String.class, padding);
    }

    public static DecimalField decimal(int group, String name, String displayName, int padding) {
        return new DecimalField(group, name, displayName, padding);
    }
}
