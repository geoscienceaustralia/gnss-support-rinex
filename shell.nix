{
  pkgs ? import ./nix {},
  installDevTools ? true
}:

let 
  projectName = "gnss-support-rinex";

  buildTools = with pkgs; [
    awscli
    docker
    maven
    openjdk8
  ];

  devTools = with pkgs; [
    niv
  ];

  env = pkgs.buildEnv {
    name = projectName + "-env";
    paths = buildTools ++ (
      if installDevTools then devTools else []
    );
  };

in
  pkgs.mkShell {
    buildInputs = [
      env
    ];
    shellHook = ''
      export PROJECT_NAME=${projectName}
      export SSL_CERT_FILE="${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
    '';
  }
